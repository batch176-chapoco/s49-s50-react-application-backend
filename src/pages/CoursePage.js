//CoursePage.js

import UserView from '../componenets/UserView';
import AdminView from '../componenets/AdminView';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';




export default function CoursePage() {

    const [allCourses, setAllCourses] = useState([])

    const fetchData = () => {
        fetch('http://localhost:4000/courses/all')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                //storing all the data to our useState allCourses
                setAllCourses(data)
            })
    }

    //it renders the function fetchData() => it gets the updated coming from the fetch
    useEffect(() => {
        fetchData()
    }, [])


    const { user } = useContext(UserContext);

    return (
        <>
            <h1>Courses</h1>

            {(user.isAdmin === true) ?

                <AdminView coursesData={allCourses} fetchData={fetchData} />

                :

                <UserView coursesData={allCourses} />
            }

        </>


    )
}