const coursesData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 45000
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 50000
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 55000
    },
]
export default coursesData;